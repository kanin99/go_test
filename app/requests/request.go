package requests

import (
	"blog/pkg/model"
	"errors"
	"fmt"
	"github.com/thedevsaddam/govalidator"
	"strings"
)

func init() {
	//示例： not_exists:users,email
	govalidator.AddCustomRule("not_exists", func(field string, rule string, message string, value interface{}) error {

		rng := strings.Split(strings.TrimPrefix(rule, "not_exists:"), ",")
		//表名
		tableName := rng[0]
		//字段名
		dbFiled := rng[1]
		//值 转换为字符串类型
		val := value.(string)

		var count int64
		model.DB.Table(tableName).Where(dbFiled+" = ?", val).Count(&count)

		if count != 0 {
			if message != "" {
				return errors.New(message)
			}

			return fmt.Errorf("%v 已经被占用", val)
		}

		return nil
	})
}

package controllers

import (
	"blog/pkg/view"
	"fmt"
	"net/http"
)

// PagesController 处理静态页面
type PagesController struct {
}

// About 关于我们页面
func (*PagesController) About(w http.ResponseWriter, r *http.Request) {
	// 4.3 表单验证不通过，显示理由
	view.Render(w, view.D{}, "pages.about")
}

// NotFound 404 页面
func (*PagesController) NotFound(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusNotFound)
	fmt.Fprint(w, "<h1>请求页面未找到 :(</h1><p>如有疑惑，请联系我们。</p>")
}

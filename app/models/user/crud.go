package user

import (
	"blog/pkg/logger"
	"blog/pkg/model"
	"blog/pkg/types"
)

// Create 创建用户，通过 User.ID 来判断是否创建成功
func (u *User) Create() (err error) {
	if err = model.DB.Create(&u).Error; err != nil {
		logger.LogError(err)
		return err
	}

	return nil
}

// Get 通过 ID 获取文章
func Get(idStr string) (User, error) {
	var user User
	id := types.StringToInt(idStr)
	if err := model.DB.First(&user, id).Error; err != nil {
		return user, err
	}
	return user, nil
}

// Get 通过 ID 获取文章
func GetByEmail(email string) (User, error) {
	var user User
	if err := model.DB.Where("`email`=?", email).First(&user).Error; err != nil {
		return user, err
	}
	return user, nil
}

// 获取全部文章
func GetAll() ([]User, error) {
	var user []User
	if err := model.DB.Find(&user).Error; err != nil {
		return user, err
	}
	return user, nil
}

package article

import (
	"blog/app/models"
	"blog/app/models/user"
	"blog/pkg/route"
)

// Article 文章模型
type Article struct {
	models.BaseModel

	Title  string `valid:"title"`
	Body   string `valid:"body"`
	UserID uint64 `gorm:"not null;index" valid:"user_id"`
	User   user.User
}

// Link 方法用来生成文章链接
func (article Article) Link() string {
	return route.Name2URL("articles.show", "id", article.GetStringID())
}

// CreatedAtDate 创建日期
func (article Article) CreatedAtDate() string {
	//2006-01-02 15:04:05 固定值，太坑了，不进源码看都不知道 ：
	//time, defined to be
	//Mon Jan 2 15:04:05 -0700 MST 2006
	return article.CreatedAt.Format("2006-01-02 15:04:05")
}

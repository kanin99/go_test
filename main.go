package main

import (
	"blog/app/middlewares"
	"blog/bootstrap"
	"blog/config"
	c "blog/pkg/config"
	"net/http"

	"github.com/gorilla/mux"
)

var router *mux.Router

func init() {
	// 初始化配置信息
	config.Initialize()
}

func main() {
	//初始化数据库
	bootstrap.SetupDB()
	// 路由初始化
	router = bootstrap.SetupRoute()

	http.ListenAndServe(":"+c.GetString("app.port"), middlewares.RemoveTrailingSlash(router))
}

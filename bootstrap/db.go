package bootstrap

import (
	"blog/app/models/article"
	"blog/app/models/user"
	config2 "blog/pkg/config"
	"blog/pkg/model"
	"gorm.io/gorm"
	"time"
)

// SetupDB 初始化数据库和 ORM
func SetupDB() {

	// 建立数据库连接池
	db := model.ConnectDB()

	// 命令行打印数据库请求
	sqlDB, _ := db.DB()

	maxIdleConnections := config2.GetInt("database.mysql.max_idle_connections")
	maxOpenConnections := config2.GetInt("database.mysql.max_open_connections")
	maxLifeSeconds := config2.GetInt("database.mysql.max_life_seconds")
	// 设置最大连接数
	sqlDB.SetMaxOpenConns(maxOpenConnections)
	// 设置最大空闲连接数
	sqlDB.SetMaxIdleConns(maxIdleConnections)
	//设置每个连接的过期时间
	sqlDB.SetConnMaxLifetime(time.Duration(maxLifeSeconds) * time.Second)

	// 创建和维护数据表结构
	migration(db)
}

func migration(db *gorm.DB) {
	db.AutoMigrate(
		&user.User{},
		&article.Article{},
	)
}
